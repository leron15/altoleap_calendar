import { VueAuthenticate } from 'vue-authenticate';


declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}
interface AltoleapCalendar {
  init?: () => any;
  [prop: string]: any;
}
declare module 'vue/types/vue' {
  interface Vue {
    $altoleapCalendar: Partial<Record<string, any>> & Vue | Partial<AltoleapCalendar> | any;
    $auth: VueAuthenticate;
  }
}