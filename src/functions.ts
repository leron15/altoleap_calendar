import { ComponentOptions } from 'vue/types/umd';
import moment from 'moment';
export function isArray<T>(input: any): input is Array<T> {
    return input instanceof Array;
}
export function isObject<T extends object>(input: any): input is T {
    return input !== null && !isArray(input) && typeof (input) === 'object';
}


export function isEmptyObject(obj: any) {
    return (
        toString.call(obj) === '[object Object]' && Object.keys(obj).length === 0
    );
}
export function merge(target: any, source: any) {
    if (!isObject<any>(target)) {
        return source;
    }

    if (isObject<any>(source)) {
        for (const prop in source) {
            const sourceValue = source[prop];

            if (prop in target) {
                merge(target[prop], sourceValue);
            } else {
                target[prop] = sourceValue;
            }
        }
    }

    return target;
}

export function mergeVueOptions(options: ComponentOptions<any>, defaults: ComponentOptions<any>) {
    const out = {
        data: {},
        methods: {},
        computed: {}
    };

    mergeVueOptionsGroup(options, defaults.data, out, out.data);
    mergeVueOptionsGroup(options, defaults.computed, out, out.computed);
    mergeVueOptionsGroup(options, defaults.methods, out, out.methods);

    return out;
}

export function mergeVueOptionsGroup(options: any, group: any, out: any, outGroup: any) {
    for (const prop in group) {
        if (options.data && prop in options.data) {
            out.data[prop] = options.data[prop];

            merge(out.data[prop], group[prop]);
        } else if (options.computed && prop in options.computed) {
            out.computed[prop] = options.computed[prop];

            merge(out.computed[prop], group[prop]);
        } else if (options.methods && prop in options.methods) {
            out.methods[prop] = options.methods[prop];

            merge(out.methods[prop], group[prop]);
        } else {
            outGroup[prop] = group[prop];
        }
    }
}

export function dsBind(bind: any) {
    return function (data: any, tag: any, value: any, asProp: any, isSync: any): any {
        if (value && value.$scopedSlots) {
            data.scopedSlots = value.$scopedSlots;
            delete value.$scopedSlots;
        }

        return bind.apply(this, arguments);
    };
}


const MONTH_NAMES = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

function getFormattedDate(
    date: Date,
    prefomattedDate?: string,
    hideYear = false
) {
    const format = moment(date);
    const day = date.getDate();
    const month = MONTH_NAMES[date.getMonth()];
    const year = date.getFullYear();
    const hours = date.getHours();
    let minutes = date.getMinutes() as string | number;

    if (minutes < 10) {
        // Adding leading zero to minutes
        minutes = `0${minutes}`;
    }

    if (prefomattedDate) {
        // Today at 10:20
        // Yesterday at 10:20
        // return `${prefomattedDate}, ${hours}:${minutes}`;
        return format.fromNow();
    }

    if (hideYear) {
        // 10. January at 10:20
        // return `${month} ${day}, ${hours}:${minutes}`;
        return format.fromNow();
    }

    // 10. January 2017. at 10:20
    return format.fromNow();
    // return `${month} ${day} ${year}, at ${hours}:${minutes}`;
}


// --- Main function
export function timeAgo(dateParam: Date) {
    if (!dateParam) {
        return null;
    }

    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(today.getTime() - DAY_IN_MS);
    const seconds = Math.round((today.getTime() - date.getTime()) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();

    if (seconds < 5) {
        return 'now';
    } else if (seconds < 60) {
        return `${seconds} seconds ago`;
    } else if (seconds < 90) {
        return 'about a minute ago';
    } else if (minutes < 60) {
        return `${minutes} minutes ago`;
    } else if (isToday) {
        return getFormattedDate(date, 'Today'); // Today at 10:20
    } else if (isYesterday) {
        return getFormattedDate(date, 'Yesterday'); // Yesterday at 10:20
    } else if (isThisYear) {
        return getFormattedDate(date, undefined, true); // 10. January at 10:20
    }

    return getFormattedDate(date); // 10. January 2017. at 10:20
}
