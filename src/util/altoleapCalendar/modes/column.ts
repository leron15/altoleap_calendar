// import { CalendarEventOverlapMode } from 'vuetify/types'
import { getOverlapGroupHandler } from './common';

const FULL_WIDTH = 100;

export interface CalendarEventVisual {
  event: any;
  columnCount: number;
  column: number;
  left: number;
  width: number;
}
export type CalendarEventOverlapMode = (events: any[], firstWeekday: number, overlapThreshold: number) => (day: CalendarDay<any, any>, dayEvents: any[], timed: boolean, reset: boolean) => CalendarEventVisual[];


export const column: CalendarEventOverlapMode = (events, firstWeekday, overlapThreshold) => {
  const handler = getOverlapGroupHandler(firstWeekday);

  return (day, dayEvents, timed, reset) => {
    const visuals = handler.getVisuals(day, dayEvents, timed, reset);

    if (timed) {
      visuals.forEach(visual => {
        visual.left = visual.column * FULL_WIDTH / visual.columnCount;
        visual.width = FULL_WIDTH / visual.columnCount;
      });
    }

    return visuals;
  };
};
