// import { CalendarEventOverlapMode } from 'vuetify/types'
import { stack } from './stack';
import { column } from './column';

export interface CalendarEventVisual {
  event: any;
  columnCount: number;
  column: number;
  left: number;
  width: number;
}
export type CalendarEventOverlapMode = (events: any[], firstWeekday: number, overlapThreshold: number) => (day: CalendarDay<any, any>, dayEvents: any[], timed: boolean, reset: boolean) => CalendarEventVisual[];


export const CalendarEventOverlapModes: Record<string, CalendarEventOverlapMode> = {
  stack,
  column,
};
