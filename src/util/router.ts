import { RouteConfig, RedirectOption } from "vue-router";
// import { kebabCase } from "lodash";

// import languages from "@/data/i18n/languages.json";

// export function layout(path: string, name: string, children: RouteConfig[]) {
//   const folder = kebabCase(name);

//   return {
//     path,
//     component: () =>
//       import(
//         /* webpackChunkName: "layout-[request]" */
//         `@/layouts/${folder}/Index`
//       ),
//     props: true,
//     children
//   };
// }

export function trailingSlash(str: string) {
  return str.endsWith("/") ? str : str + "/";
}

// export const languagePattern = languages.map(lang => lang.locale).join("|");
// export const languageRegexp = new RegExp("^(" + languagePattern + ")$");

// // Matches any language identifier
// export const genericLanguageRegexp = /[a-z]{2,3}|[a-z]{2,3}-[a-zA-Z]{4}|[a-z]{2,3}-[A-Z]{2,3}/;

// export const preferredLanguage =
//   typeof document === "undefined"
//     ? "en"
//     : window.localStorage.getItem("currentLanguage") ||
//     navigator.languages.find((l: any) => l.match(languageRegexp)) ||
//     "en";

// export function redirect(redirect: RedirectOption) {
//   return { path: "*", redirect };
// }

// export function root(children: RouteConfig[]) {
//   return [
//     layout(`/:lang(${languagePattern})`, "Root", children),
//     {
//       path: `/:lang(${genericLanguageRegexp.source})/*`,
//       redirect: (to: any) =>
//         trailingSlash(`/${preferredLanguage}/${to.params.pathMatch || ""}`)
//     },
//     {
//       // The previous one doesn't match if there's no slash after the language code
//       path: `/:lang(${genericLanguageRegexp.source})`,
//       redirect: () => `/${preferredLanguage}/`
//     },
//     redirect((to: any) => trailingSlash(`/${preferredLanguage}${to.path}`))
//   ];
// }
// type RouteType = Omit<RouteConfig, 'component'> & { file?: string; };
// export function route(config: RouteType): RouteConfig {
//   const folder = (config.file || `${kebabCase(config.name)}`).toLowerCase();
//   console.log({ folder });

//   return {
//     component: () => import(`@/views/${folder}/Index.vue`),
//     name: config.name,
//     path: config.path
//   };
// }
