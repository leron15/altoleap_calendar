export const set = (property: string | number) => (
  store: { [x: string]: any },
  payload: any
) => (store[property] = payload);
export const toggle = (property: string | number) => (store: {
  [x: string]: any;
}) => (store[property] = !store[property]);
export const push = (property: string | number) => (
  store: { [x: string]: any[] },
  payload: any
) => store[property].push(payload);
