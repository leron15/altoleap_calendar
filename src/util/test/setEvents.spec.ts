import { Calendar, ScheduleInput, Day, Op } from "../newCalendar";
import { setUTCPartsToDate } from '../dateHelpers';

function parseEvents(
    events: any[]
): { data: any; schedule: ScheduleInput<any>; }[] {
    // if (events.length) return [];
    return events.map((event: any, index) => {
        const times =
            new Day(setUTCPartsToDate(new Date(event.date.start))).format("HH:mm") ==
                "00:00"
                ? undefined
                : [
                    new Day(setUTCPartsToDate(new Date(event.date.start))).format(
                        "HH:mm"
                    ),
                ];
        console.log({
            times,
            time: new Day(setUTCPartsToDate(new Date(event.date.start))).format(
                "HH:mm"
            ),
        });

        return {
            data: event,
            schedule: {
                on: new Day(setUTCPartsToDate(new Date(event.date.start))),
                times,
                duration: new Day(new Date(event.date.start)).minutesBetween(
                    new Day(new Date(event.date.end)),
                    Op.NONE
                ),
                durationUnit: "minutes",
                dayOfMonth: [
                    new Day(setUTCPartsToDate(new Date(event.date.start))).dayOfMonth!,
                ],
                // dayOfWeek: [
                //   new Day(setUTCPartsToDate(new Date(event.date.start))).dayOfWeek!,
                // ],
            },
        };
    });
}
describe('Calendar setEvents to be parsed as a type of event', () => {
    const calendar = Calendar.months<any, any>();
    const events = [{
        title: "Weekly Meeting",
        color: "#009688",
        date: {
            start: "2020-07-16T00:00:00Z",
            end: "2020-07-17T00:00:00Z",
            timezone: "America/New_York",
        },
    }];

    expect(calendar.setEvents(parseEvents(events))).toEqual(expect.anything());
});