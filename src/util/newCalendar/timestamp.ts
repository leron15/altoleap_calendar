import { Iterate, IterateAction } from 'iteratez';

import { Constants } from './Constants';
import { CalendarDay } from './CalendarDay';
import { Day } from './Day';
import { Identifier, IdentifierInput } from './Identifier';
import { Units } from './Units';
import { DaySpan, DaySpanBounds } from './DaySpan';
import { Op } from './Operation';
import { Functions as fn } from './Functions';
import { CalendarEvent } from './CalendarEvent';
import { setUTCPartsToDate, setPartsToUTCDate } from '../dateHelpers';

export const Meta = {
	[Units.DAY]:
	{
		getStart(around: Day, size: number, focus: number): Day {
			return around.startOf('day').add('day', -Math.floor(size * focus));
		},
		getEnd(start: Day, size: number, focus: number): Day {
			return start.add('day', size - 1).endOf('day');
		},
		moveStart(day: Day, amount: number): Day {
			return day.add('day', amount);
		},
		moveEnd(day: Day, amount: number): Day {
			return day.add('day', amount);
		},
		defaultInput: undefined
	},
	[Units.WEEK]:
	{
		getStart(around: Day, size: number, focus: number): Day {
			return around.startOf('week').add('week', -Math.floor(size * focus));
		},
		getEnd(start: Day, size: number, focus: number): Day {
			return start.add('week', size - 1).endOf('week');
		},
		moveStart(day: Day, amount: number): Day {
			return day.add('week', amount);
		},
		moveEnd(day: Day, amount: number): Day {
			return day.add('week', amount);
		},
		defaultInput: undefined
	},
	[Units.MONTH]:
	{
		getStart(around: Day, size: number, focus: number): Day {
			return around.startOf('month').add('month', -Math.floor(size * focus));
		},
		getEnd(start: Day, size: number, focus: number): Day {
			return start.add('month', size - 1).endOf('month');
		},
		moveStart(day: Day, amount: number): Day {
			return day.add('month', amount);
		},
		moveEnd(day: Day, amount: number): Day {
			return day.startOf('month').add('month', amount).endOf('month');
		},
		defaultInput: { fill: true }
	},
	[Units.YEAR]:
	{
		getStart(around: Day, size: number, focus: number): Day {
			return around.startOf('year').add('year', -Math.floor(size * focus));
		},
		getEnd(start: Day, size: number, focus: number): Day {
			return start.add('year', size - 1).endOf('year');
		},
		moveStart(day: Day, amount: number): Day {
			return day.add('year', amount);
		},
		moveEnd(day: Day, amount: number): Day {
			return day.add('year', amount);
		},
		defaultInput: { fill: true }
	},
	5: {
		getStart(around: Day, size: number, focus: number): Day {
			return around.startOf('month').add('month', -Math.floor(size * focus));
		},
		getEnd(start: Day, size: number, focus: number): Day {
			return start.add('month', size - 1).endOf('month');
		},
		moveStart(day: Day, amount: number): Day {
			return day.add('month', amount);
		},
		moveEnd(day: Day, amount: number): Day {
			return day.startOf('month').add('month', amount).endOf('month');
		},
		defaultInput: { fill: true }
	}
};

export function forType(type: Units, size: number = 1, around: Day = Day.today(), focus: number = 0.49999, input?: any): any[] {
	const meta: any = Meta[type];
	const start: Day = meta.getStart(around, size, focus);
	const end: Day = meta.getEnd(start, size, focus);

	// return {start, end, type, size, meta.moveStart, meta.moveEnd, input || meta.defaultInput};
	// const minimumSize = fn.coalesce(input.minimumSize, 0);
	const minimumSize = 0;
	// console.log(fn.coalesce(input.minimumSize, 0));

	const span = new DaySpan(start, end);
	const filled = new DaySpan(start, end);
	const days: CalendarDay[] = [];
	const daysBetween: number = filled.days(Op.UP);
	const total: number = Math.max(minimumSize, daysBetween);
	let current: Day = filled.start;

	for (let i = 0; i < total; i++) {
		let day: CalendarDay = days[i];

		if (!day || !day.sameDay(current)) {
			day = new CalendarDay(current.date);

			if (i < days.length) {
				days.splice(i, 1, day);
			}
			else {
				days.push(day);
			}
		}

		day.inCalendar = span.contains(day);

		current = current.next();
	}
	if (days.length > total) {
		days.splice(total, days.length - total);
	}
	/**
	* Updates the days with the current day via [[CalendarDay.updateCurrent]].
	*
	* @param Day.today() The new current day.
	*/
	iterateDays(days).each(d => d.updateCurrent(Day.today()));

	return days;


}
export function iterateDays(days: CalendarDay[]): Iterate<CalendarDay, number, any> {
	return new Iterate<CalendarDay, number, any>(iterator => {
		// const days: CalendarDay[] = days;

		for (let i = 0; i < days.length; i++) {
			switch (iterator.act(days[i], i)) {
				case IterateAction.STOP:
					return;
			}
		}
	});
}

export function getWeekViewDays(weeks: number = 1, around: any = Day.today(), focus: number = 0.4999, input?: any): CalendarDay<any, any>[] {
	return forType(Units.WEEK, weeks, around, focus, input);
}

export function getDayViewDays(days: number = 1, around: Day = Day.today(), focus: number = 0.4999, input?: any): any[] {
	return forType(Units.DAY, days, around, focus, input);
}
export function getMonthViewDays(months: number = 1, around: Day = Day.today(), focus: number = 0.4999, input?: any): any[] {
	return forType(Units.MONTH, months, around, focus, input);
}

export function getYearViewDays(years: number = 1, around: Day = Day.today(), focus: number = 0.4999, input?: any): any[] {
	return forType(Units.MONTH, years, around, focus, input);
}

export function getScheduleViewDays(scheduleDays: number = 1, around: Day = Day.today(), focus: number = 0.0000, input?: any): any[] {
	return forType(Units.DAY, scheduleDays, around, focus, input);
}
export function getDayIdentifier(day: Day | CalendarDay): IdentifierInput | null | any {
	return day.dayIdentifier;
}
export function getTimeIdentifier(day: Day | CalendarDay): IdentifierInput | null | any {
	return day.timeIdentifier;
}

export function getTimestampIdentifier(day: Day | CalendarDay): any {
	return parseInt(getDayIdentifier(day), 10) * Constants.OFFSET_TIME + parseInt(getTimeIdentifier(day), 10);
}

export function copyDay(day: Day | CalendarDay): Day | CalendarDay | null {
	const newDay = Day.fromDate(day.toDate());

	return newDay;
}

export function getWeekdaySkips(weekdays: number[]): number[] {
	const skips: number[] = [1, 1, 1, 1, 1, 1, 1];
	const filled: number[] = [0, 0, 0, 0, 0, 0, 0];
	for (let i = 0; i < weekdays.length; i++) {
		filled[weekdays[i]] = 1;
	}
	for (let k = 0; k < Constants.DAYS_IN_WEEK; k++) {
		let skip = 1;
		for (let j = 1; j < Constants.DAYS_IN_WEEK; j++) {
			const next = (k + j) % Constants.DAYS_IN_WEEK;
			if (filled[next]) {
				break;
			}
			skip++;
		}
		skips[k] = filled[k] * skip;
	}

	return skips;
}
export function createDayList(
	start: Day | CalendarDay,
	end: Day | CalendarDay,
	now: Day | CalendarDay,
	weekdaySkips: number[],
	max = 42,
	min = 0
): (Day | CalendarDay)[] {
	const stop = getDayIdentifier(end);
	const days: Day[] = [];
	let current = copyDay(start)!;
	let currentIdentifier = 0;
	let stopped = currentIdentifier === stop;

	if (stop < getDayIdentifier(start)) {
		throw new Error('End date is earlier than start date.');
	}

	while ((!stopped || days.length < min) && days.length < max) {
		currentIdentifier = getDayIdentifier(current);
		stopped = stopped || currentIdentifier === stop;
		if (weekdaySkips[current!.dayOfWeek!] === 0) {
			current = current.add('day', 1);
			continue;
		}
		const day = copyDay(current)!;
		//   updateFormatted(day);
		//   updateRelative(day, now);
		days.push(day);
		//   current = relativeDays(current, nextDay, weekdaySkips[current.weekday]);
	}

	if (!days.length) throw new Error('No dates found using specified start date, end date, and weekdays.');

	return days;
}

export function createIntervalList(timestamp: Day, first: number,
	minutes: number, count: number, now?: Day): Day[] {
	const intervals: Day[] = [];

	for (let i = 0; i < count; i++) {
		const mins = first + (i * minutes);
		const int = copyDay(timestamp);
		intervals.push(updateMinutes(int!, mins, now));
	}

	return intervals;
}

export function updateMinutes(timestamp: Day, minutes: number, now?: Day): Day {
	// timestamp.hasTime = true;
	// timestamp.hour = Math.floor(minutes / Constants.MINUTES_IN_HOUR);
	// timestamp.minute = minutes % Constants.MINUTES_IN_HOUR;
	// timestamp.time = getTime(timestamp);
	const day = new Date(timestamp.toDate());
	day.setHours(Math.floor(minutes / Constants.MINUTES_IN_HOUR));
	day.setMinutes(minutes % Constants.MINUTES_IN_HOUR);

	// if (now) {
	//   updateRelative(timestamp, now, true);
	// }
	timestamp = new Day(day);
	// console.count();
	// console.log({ timestamp });
	// console.log(Math.floor(minutes / Constants.MINUTES_IN_HOUR));



	return timestamp;
}

export function validateTimestamp(input: any): input is TimestampInput {
	return (typeof input === 'number' && isFinite(input)) ||
		(typeof input === 'string') ||
		(input instanceof Date) ||
		(input instanceof Day);
}
export type TimestampInput = number | string | Date | Day;
export function parseDay(input: TimestampInput, required?: false, now?: Day): Day | null;
export function parseDay(input: TimestampInput, required: true, now?: Day): Day;
export function parseDay(input: TimestampInput, required = false, now?: Day): Day | null {
	console.log({ input });

	if (typeof input === 'number' && isFinite(input)) {
		input = new Date(input);
	}

	if (input instanceof Date) {
		const date: Day = Day.fromDate(input)!;

		// if (now) {
		//   updateRelative(date, now, date.hasTime)
		// }

		return date;
	}
	if (!(input instanceof Day)) {
		if (required) {
			throw new Error(`${input} is not a valid timestamp. It must be a Date, number of seconds since Epoch, or a string in the format of YYYY-MM-DD or YYYY-MM-DD hh:mm. Zero-padding is optional and seconds are ignored.`);
		}

		return null;
	}

	if (typeof input !== 'string') {
		if (required) {
			throw new Error(`${input} is not a valid timestamp. It must be a Date, number of seconds since Epoch, or a string in the format of YYYY-MM-DD or YYYY-MM-DD hh:mm. Zero-padding is optional and seconds are ignored.`);
		}
		return null;
	}
	if (typeof input === 'string') {
		const date: Day = Day.fromString(input)!;
		return date;
	}

	return input;
}

export function parseEvent(
	input: any,
	index: number,
	useUtc: boolean = false,
	timed = false,
	category: string | false = false
): any {
	const startProperty = new Date(input.date.start);
	const endProperty = new Date(input.date.end);
	const startInput = new Date(setUTCPartsToDate(startProperty));
	const endInput = new Date(setUTCPartsToDate(endProperty));
	const startParsed: Day = new Day(startInput);
	const endParsed: Day = endInput ? new Day(endInput) : startParsed;
	const start: Day =
		// isTimedless(startInput)
		//   ? updateHasTime(startParsed, timed)
		//   :
		startParsed;
	const end: Day =
		//  isTimedless(endInput)
		//   ? updateHasTime(endParsed, timed)
		//   :
		endParsed;
	const startIdentifier: number = Number(start.dayIdentifier!);
	const startTimestampIdentifier: number = getTimestampIdentifier(start);
	const endIdentifier: number = Number(end.dayIdentifier!);
	const endOffset: number = start.hasTime() ? 0 : 2359;
	const endTimestampIdentifier: number =
		getTimestampIdentifier(end) + endOffset;
	const allDay: boolean = !start.hasTime();
	const time: DaySpan = new DaySpan(start, end);
	const row: number = 0;
	const col: number = 0;

	const getTimeBounds = (day: Day, dayHeight: number = 1, dayWidth: number = 1, columnOffset: number = 0.1, clip: boolean = true, offsetX: number = 0, offsetY: number = 0): DaySpanBounds => {
		return time.getBounds(day, dayHeight, dayWidth, col * columnOffset, clip, offsetX, offsetY);
	};


	return {
		input,
		start,
		startIdentifier,
		startTimestampIdentifier,
		end,
		endIdentifier,
		endTimestampIdentifier,
		allDay,
		row,
		col,
		getTimeBounds,
		time,
		index,
		category,
	};
}

export const OFFSET_TIME = 10000;

export function isEventOn(event: any, dayIdentifier: IdentifierInput): boolean {
	return (
		dayIdentifier >= event.startIdentifier &&
		dayIdentifier <= event.endIdentifier &&
		Number(dayIdentifier) * OFFSET_TIME !== event.endTimestampIdentifier
	);
}

export function isEventStart(
	event: any,
	day: Day | CalendarDay<any, any>,
	dayIdentifier: IdentifierInput,
	firstWeekday: number
): boolean {
	return (
		dayIdentifier === event.startIdentifier ||
		(firstWeekday === day.dayOfWeek && isEventOn(event, dayIdentifier))
	);
}

export function isEventOverlapping(
	event: any,
	startIdentifier: IdentifierInput,
	endIdentifier: IdentifierInput
): boolean {
	return (
		startIdentifier <= event.endIdentifier &&
		endIdentifier >= event.startIdentifier
	);
}


class Calendar<T, M> {

	// public constructor(start: Day, end: Day, type: Units, size: number, moveStart: CalendarMover, moveEnd: CalendarMover, input?: CalendarInput<T, M>)
	// {
	//   this.span = new DaySpan(start, end);
	//   this.filled = new DaySpan(start, end);
	//   this.type = type;
	//   this.size = size;
	//   this.moveStart = moveStart;
	//   this.moveEnd = moveEnd;

	//   if (fn.isDefined(input))
	//   {
	//     this.set( input );
	//   }
	//   else
	//   {
	//     this.refresh();
	//   }
	// }

	// public static forType<T, M>(type: Units, size: number = 1, around: Day = Day.today(), focus: number = 0.49999, input?: CalendarInput<T, M>): Calendar<T, M>
	// {
	//   const meta: any = this.TYPES[ type ];
	//   const start: Day = meta.getStart( around, size, focus );
	//   const end: Day = meta.getEnd( start, size, focus );

	//   return new Calendar<T, M>(start, end, type, size, meta.moveStart, meta.moveEnd, input || meta.defaultInput);
	// }
	public static TYPES: any = {
		[Units.DAY]:
		{
			getStart(around: Day, size: number, focus: number): Day {
				return around.startOf('day').add('day', -Math.floor(size * focus));
			},
			getEnd(start: Day, size: number, focus: number): Day {
				return start.add('day', size - 1).endOf('day');
			},
			moveStart(day: Day, amount: number): Day {
				return day.add('day', amount);
			},
			moveEnd(day: Day, amount: number): Day {
				return day.add('day', amount);
			},
			defaultInput: undefined
		},
		[Units.WEEK]:
		{
			getStart(around: Day, size: number, focus: number): Day {
				return around.startOf('week').add('week', -Math.floor(size * focus));
			},
			getEnd(start: Day, size: number, focus: number): Day {
				return start.add('week', size - 1).endOf('week');
			},
			moveStart(day: Day, amount: number): Day {
				return day.add('week', amount);
			},
			moveEnd(day: Day, amount: number): Day {
				return day.add('week', amount);
			},
			defaultInput: undefined
		},
		[Units.MONTH]:
		{
			getStart(around: Day, size: number, focus: number): Day {
				return around.startOf('month').add('month', -Math.floor(size * focus));
			},
			getEnd(start: Day, size: number, focus: number): Day {
				return start.add('month', size - 1).endOf('month');
			},
			moveStart(day: Day, amount: number): Day {
				return day.add('month', amount);
			},
			moveEnd(day: Day, amount: number): Day {
				return day.startOf('month').add('month', amount).endOf('month');
			},
			defaultInput: { fill: true }
		},
		[Units.YEAR]:
		{
			getStart(around: Day, size: number, focus: number): Day {
				return around.startOf('year').add('year', -Math.floor(size * focus));
			},
			getEnd(start: Day, size: number, focus: number): Day {
				return start.add('year', size - 1).endOf('year');
			},
			moveStart(day: Day, amount: number): Day {
				return day.add('year', amount);
			},
			moveEnd(day: Day, amount: number): Day {
				return day.add('year', amount);
			},
			defaultInput: { fill: true }
		}
	};
}