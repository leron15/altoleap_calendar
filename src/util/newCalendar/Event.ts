
import { Schedule, ScheduleInput } from './Schedule';
import { Parse } from './Parse';
import { Day } from './Day';
import { Op } from './Operation';
import { durations } from './DayFunctions';
import { Constants } from './Constants';
import { DaySpan } from './DaySpan';
import { IterateAction, Iterate } from 'iteratez';
import { Time } from './Time';


type Iter = Iterate<any, number, Event<any>>;

export interface DateClass {
    start?: Date | string;
    end?: Date | string;
    timezone?: string;
}

export interface Recurrence {
    enabled?: boolean;
    rules?: string[];
}

export interface CalendarOccurrence {
    id: number | null;
    title: string;
    description: string;
    location: string;
    color: string;
    date: DateClass;
    recurrence: Recurrence;
    cancelled: boolean;
    task_id: string;
    event: number;
    calendar: number;
    participants: Participant[] | Attendee[] | { attendee: number; }[] | any;
    original_start: Date | string;
    original_end: Date | string;
    owner: string;
    reminder: Reminder | null;
}


export interface Reminder {
    id?: number;
    enabled: boolean;
    minutes: number;
}

export interface Participant {
    attendee: Attendee;
    rsvp_status: number;
}
export interface Attendee {
    id: number;
    first_name: string;
    last_name: string;
}

/**
 * The input which can be passed to the calendar when adding a schedule and event.
 *
 * @typeparam T The type of data stored in the [[Event]] class.
 * @typeparam M The type of metadata stored in the schedule.
 */
export interface EventInput<T, M> {
    id?: any;
    data?: T;
    schedule: ScheduleInput<M> | Schedule<M>;
}
// export interface EventInput<T> {
//     data?: T;
// }

/**
 * A pairing of a user specified event object and the schedule which defines
 * when it occurs on a calendar.
 *
 * @typeparam T The type of data stored in the [[Event]] class.
 * @typeparam M The type of metadata stored in the schedule.
 */
export class Event<T extends CalendarOccurrence>
{

    /**
     * User specified ID which can be used to find or remove this event from a
     * Calendar.
     */
    public id: any;

    /**
     * User specified object which describes this event.
     */
    public data: T; //Calendaroccurrence

    /**
     * The schedule which defines when this event occurs.
     */
    public schedule: Schedule<T>;

    /**
     * If the event is visible on the calendar.
     */
    public visible: boolean;

    /**
     * The length of events in this schedule.
     */
    public duration: number;


    /**
    * The number of days an event in this schedule lasts PAST the starting day.
    * If this is a full day event with a duration greater than zero this value
    * will be greater than one. If this event occurs at a specific time with a
    * given duration that is taken into account and if it passes over into the
    * next day this value will be greater than one. This value is used to look
    * back in time when trying to figure out what events start or overlap on a
    * given day.
    */
    public durationInDays: number;

    /**
     * The earliest an event can occur in the schedule, or `null` if there are no
     * restrictions when the earliest event can occur. This day is inclusive.
     */
    public start: Day;

    /**
     * The latest an event can occur in the schedule, or `null` if there are no
     * restrictions when the latest event can occur. This day is inclusive.
     */
    public end: Day;

    /**
     * Creates a new event.
     *
     * @param schedule The schedule which defines when the event occurs.
     * @param data User specified object which describes this event.
     * @param id User specified ID which identifies this event.
     */
    public constructor(data: any, id?: any, visible: boolean = true) {
        this.schedule = Parse.schedule<any>(data.schedule, null as any);
        this.data = data;
        this.id = id;
        this.visible = visible;
        this.setData();
        this.updateDurationInDays();
    }

    public setData() {
        this.start = Parse.day(this.data.date.start!)!;
        this.end = Parse.day(this.data.date.end!)!;



        this.duration = this.start!.minutesBetween(
            this.end!,
            Op.NONE
        );
    }

    public matchesRange(start: Day, end: Day): boolean {
        if (this.start && end.isBefore(this.start)) {
            return false;
        }

        if (this.end && start.isAfter(this.end)) {
            return false;
        }

        return true;
    }

    /**
     * Determines whether the given day lies between the earliest and latest
     * valid day in the schedule.
     *
     * @param day The day to test.
     * @returns `true` if the day lies in the schedule, otherwise `false`.
     * @see [[start]]
     * @see [[end]]
    */
    public matchesSpan(day: Day): boolean {
        return (this.start === null || day.isSameOrAfter(this.start!)) &&
            (this.end === null || day.isBefore(this.end!));
    }

    /**
     * Updates the [[Schedule.durationInDays]] variable based on the
     * [[Schedule.lastTime]] (if any), the [[Schedule.duration]] and it's
     * [[Schedule.durationUnit]].
     */
    public updateDurationInDays(): this {
        const start: number = this.start ? this.start.asTime().toMilliseconds() : 0;
        const duration: number = this.duration * (durations['minute'] || 0);
        const exclude: number = Constants.MILLIS_IN_DAY;
        const day: number = Constants.MILLIS_IN_DAY;

        this.durationInDays = Math.max(0, Math.ceil((start + duration - exclude) / day));

        return this;
    }


    /**
     * Returns a span of time for a schedule with full day events starting on the
     * start of the given day with the desired duration in days or weeks.
     *
     * @param day The day the span starts on.
     * @returns The span of time starting on the given day.
     */
    public getFullSpan(day: Day): DaySpan {
        const start: Day = day.startOf('day');
        const end: Day = start.add('minute', this.duration);

        return new DaySpan(start, end);
    }

    /**
  * Returns a span of time starting on the given day at the given day with the
  * duration specified on this schedule.
  *
  * @param day The day the span starts on.
  * @param time The time of day the span starts.
  * @returns The span of time calculated.
  */
    public getTimeSpan(day: Day, time?: Time): DaySpan {
        const start: Day = day;
        const end: Day = start.add('minute', this.duration);

        return new DaySpan(start, end);
    }

    /**
     * Returns whether the events in the schedule are all day long or start at
     * specific times. Full day events start at the start of the day and end at
     * the start of the next day (if the duration = `1` and durationUnit = 'days').
     * Full day events have no times specified and should have a durationUnit of
     * either `days` or `weeks`.
     */
    public isFullDay(): boolean {
        console.log(this.start.hour, this.start.minute);

        return this.start.hour == 0 && this.start.minute == 0 && this.durationInDays == 1;
    }

    /**
  * Sets whether this schedule is a full day event if it is not already. If
  * this schedule is a full day event and `false` is passed to this function
  * a single timed event will be added based on `defaultTime`. If this schedule
  * has timed events and `true` is passed to make the schedule full day, the
  * timed events are removed from this schedule. If the durationUnit is not the
  * expected unit based on the new full day flag - the duration is reset to 1
  * and the duration unit is set to the expected unit.
  *
  * @param fullDay Whether this schedule should represent a full day event or
  *    timed events.
  * @param defaultTime If `fullDay` is `false` and this schedule is currently
  *    a full day event - this time will be used as the time of the first event.
  */
    // public setFullDay(fullDay: boolean = true, defaultTime: TimeInput = '08:00'): this {
    //     if (fullDay !== this.isFullDay()) {
    //         if (fullDay) {
    //             this.times = [];

    //             if (this.durationUnit !== 'day') {
    //                 this.duration = 1;
    //                 this.durationUnit = 'day';
    //             }
    //         }
    //         else {
    //             this.times = [Parse.time(defaultTime)];

    //             if (this.durationUnit !== 'hour') {
    //                 this.duration = 1;
    //                 this.durationUnit = 'hour';
    //             }
    //         }
    //     }

    //     return this;
    // }

    public iterateSpans(day: Day, covers: boolean = false): Iter {
        return new Iterate<DaySpan, number, Schedule<any>>(iterator => {
            let current: Day = day;
            let lookBehind: number = covers ? this.durationInDays : 0;
            let key: number = 0;

            // If the events start at the end of the day and may last multiple days....
            if (this.isFullDay()) {
                console.log('Is Full Day');

                // If the schedule has events which span multiple days we need to look
                // backwards for events that overlap with the given day.
                while (lookBehind >= 0) {
                    // If the current day matches the schedule rules...
                    if (this.matchesSpan(current)) {
                        // Build a DaySpan with the given start day and the schedules duration.
                        const span: DaySpan = this.getFullSpan(current);

                        // If that dayspan intersects with the given day, it's a winner!
                        if (span.matchesDay(day)) {
                            switch (iterator.act(span, key++)) {
                                case IterateAction.STOP:
                                    return;
                            }
                        }
                    }

                    current = current.prev();
                    lookBehind--;
                }
            }
            // This schedule has events which start at certain times
            else {
                console.log('Is Not Full Day');

                // If the schedule has events which span multiple days we need to look
                // backwards for events that overlap with the given day.
                while (lookBehind >= 0) {
                    // If the current day matches the schedule rules...
                    if (this.matchesSpan(current)) {
                        // Iterate through each daily occurrence in the schedule...
                        // for (const time of this.times) {
                        const span: DaySpan = this.getTimeSpan(current);

                        // If the event intersects with the given day and the occurrence
                        // has not specifically been excluded...
                        if (span.matchesDay(day)) {
                            switch (iterator.act(span, key++)) {
                                case IterateAction.STOP:
                                    return;
                            }
                        }
                        // }
                    }
                    // else {
                    // The current day does not match the schedule, however the schedule
                    // might have moved/random event occurrents on the current day.
                    // We only want the ones that overlap with the given day.
                    // this.iterateIncludeTimes(current, day).each((span, i, timeIterator) => {
                    //     switch (iterator.act(span, key++)) {
                    //         case IterateAction.STOP:
                    //             timeIterator.stop();
                    //             break;
                    //     }
                    // });

                    //     if (iterator.action === IterateAction.STOP) {
                    //         return;
                    //     }
                    // }

                    lookBehind--;

                    // Generating current.prev() is costly.
                    // Avoid generating it if looping condition is no longer satisfied.
                    if (lookBehind >= 0) {
                        current = current.prev();
                    }
                }
            }
        });
    }




}
