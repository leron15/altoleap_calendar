import './VCalendarWeekly.sass';

// import props from "@/util/calendar/props";
// import CalendarBase from "@/mixins/base";
import VCalendarWeekly from "../WeekView/Index.vue";

// import { convertToUnit } from "@/util/helpers";
// import {
//   CalendarDay as Day,
//   parseDay,
//   createDayList,
//   Functions as fn,
//   forType,
//   Units,
//   getWeekViewDays,
//   getDayIdentifier,
//   DaySpan,
// } from "altoleap-calendar-lib/src";
// import { mapActions } from "vuex";

export default VCalendarWeekly.extend({
  name: 'v-calendar-monthly',

  computed: {
    staticClass(): string {
      return 'v-calendar-monthly v-calendar-weekly';
    },
    // parsedStart (): CalendarTimestamp {
    //   return getStartOfMonth(parseTimestamp(this.start, true))
    // },
    // parsedEnd (): CalendarTimestamp {
    //   return getEndOfMonth(parseTimestamp(this.end, true))
    // },
  },

});
