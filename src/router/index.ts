import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import scroll from './scroll-behaviour';
import { Vuetify } from 'vuetify/types';

import { trailingSlash } from "@/util/router";


Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  // {
  //   path: "/",
  //   name: "Home",
  //   redirect: { name: 'calendar' }
  // },
  {
    path: "/calendar",
    alias: '/',
    name: "calendar",
    component: () => import("@/views/Calendar.vue"),
    children: [
      {
        path: "r/eventedit",
        name: "calendarEventEdit",
        component: () => import("@/components/CalendarEventDialog.vue"),
      }
    ]

  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue")
  }
];

export function createRouter(vuetify: Vuetify, store?: any, i18n?: any) {
  const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
    scrollBehavior: (...args: any[]): any => scroll(vuetify, ...args)
  });

  router.beforeEach((to, from, next) => {
    return to.path.endsWith('/') ? next() : next(trailingSlash(to.path));
  });
  return router;
}
