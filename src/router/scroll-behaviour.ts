// import goTo from 'vuetify/src/services/goto';
import { GoToOptions } from 'vuetify/types/services/goto';
// import { waitForReadystate } from '@/util/componentHelpers';
import { IN_BROWSER } from '@/util/globals';
import { Vuetify } from 'vuetify/types';
// import { Route } from 'vue-router';

export default async function (vuetify: Vuetify, to?: any, _?: any, savedPosition?: any) {
	if (!IN_BROWSER) return;

	let scrollTo: any = 0;

	if (to.hash) scrollTo = to.hash;
	else if (savedPosition) scrollTo = savedPosition.y;

	// TODO: https://github.com/vuejs/vue-router/pull/3199
	// scroll-behavior is not called on
	// load handled in views/Page.vue
	return new Promise((resolve, reject) => {
		// Options 1
		const options: GoToOptions = {};

		if (!scrollTo) options.duration = 0;

		window.requestAnimationFrame(async () => {
			vuetify.framework
				.goTo(scrollTo, options)
				.catch(reject)
				.finally(resolve);
		});
	});
}
