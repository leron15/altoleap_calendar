import { VueConstructor } from 'vue';
// import * as ComponentMap from '@/components';
import { mergeVueOptions, dsBind } from './functions';
import { registerPlugins } from '@/plugins';
import { default as Component } from './component';
// import { dsBind } from './main';


const install = (Vue: VueConstructor, options: any) => {
    // for (const componentName in ComponentMap) {
    //     Vue.component(componentName, (ComponentMap as any)[componentName]);
    // }
    registerPlugins(Vue);


    // $altoleapCalendar is just another reactive component
    const $altoleapCalendar: AltoleapCalendar = new Vue(options
        ? mergeVueOptions(options, Component)
        : Component);




    // // allow directives to access $altoleapCalendar
    // Vue.$altoleapCalendar = $altoleapCalendar;

    // // allow components to access $altoleapCalendar
    Vue.prototype.$altoleapCalendar = $altoleapCalendar;

    Vue.prototype._b = dsBind(Vue.prototype._b);

    $altoleapCalendar.init();

};
export default install;
