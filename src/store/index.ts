import Vue from "vue";
import Vuex from "vuex";

// Modules
import * as modules from "./modules";

// plguins
import { Calendar } from './plugins/calendar';
Vue.use(Vuex);
export function createStore() {
  const store = new Vuex.Store({
    modules,
    plugins: [Calendar]
  });
  // store.subscribe(mutation => {
  //   if (!mutation.type.startsWith('user/')) return

  //   store.dispatch('user/update', mutation)
  // })

  store.dispatch('calendar/init');
  return store;
}

export const ROOT_DISPATCH = Object.freeze({ root: true });
