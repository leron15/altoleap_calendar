import { split } from 'lodash';
import { Units } from 'altoleap-calendar-lib/src';

export const Calendar = (store: any) => {
    // called when the store is initialized
    store.subscribe((mutation: any, state: any) => {
        // called after every mutation.
        // The mutation comes in the format of `{ type, payload }`.
        if (!mutation.type.startsWith('calendar/')) return;
        const mutator = mutation.type.split('/')[1];
        if (mutator === 'SET_TYPE') {
            switch (state.calendar.type) {
                case Units.DAY:
                    store.dispatch('calendar/forType', {
                        type: Units.DAY,
                        size: 1,
                        around: state.calendar.start,
                        focus: 0.49999
                    }, null);
                    break;
                case Units.WEEK:
                    store.dispatch('calendar/forType', {
                        type: Units.WEEK,
                        size: 1,
                        around: state.calendar.start,
                        focus: 0.49999
                    }, null);
                    break;
                case Units.MONTH:
                    store.dispatch('calendar/forType', {
                        type: Units.MONTH,
                        size: 1,
                        around: state.calendar.start,
                        focus: 0.49999
                    }, null);
                    break;
                default:
                    store.dispatch('calendar/forType', {
                        type: Units.WEEK,
                        size: 1,
                        around: state.calendar.start,
                        focus: 0.49999
                    }, null);
                    return;
            }
        }

    });
    store.subscribeAction((action: any, state: any) => {
        if (!action.type.startsWith('calendar/')) return;
        // console.log(action.type);
        // console.log(action.payload);
        // console.log('log', state);
        // called after every mutation.
        // The mutation comes in the format of `{ type, payload }`.
        //   if (mutation.type === 'UPDATE_DATA') {
        //     socket.emit('update', mutation.payload)
        //   }
    });
};