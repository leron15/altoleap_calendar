import { make } from "vuex-pathify";
import { ROOT_DISPATCH } from '@/store';
// import { Calendar, Constants, Units, DaySpan, Day, CalendarDay, Meta, iterateDays, Op, Functions as fn } from 'altoleap-calendar-lib/src';
import { Calendar, Constants, Units, DaySpan, Day, CalendarDay, Op, Functions as fn, Time } from 'altoleap-calendar-lib/src';


// const calendar = Calendar.months()
const state = {
    readOnly: false,
    today: Day.today(),
    timeout: null,
    refreshInterval: Constants.MILLIS_IN_MINUTE,
    days: [] as CalendarDay<any, any>[],
    span: {} as DaySpan,
    filled: {} as DaySpan,
    type: Units.WEEK,
    start: Day.today(),
    end: Day.today(),
    calendar: Calendar.weeks<any, any>(),
    tomorrow: Day.tomorrow(),
    now: Day.now(),

};


const mutations = make.mutations(state);

const actions = {
    init: async ({ dispatch }: any) => {
        // dispatch('messages/fetch', null, ROOT_DISPATCH)
        // dispatch('user/fetch', null, ROOT_DISPATCH);
        dispatch('forType', {
            type: Units.WEEK,
            size: 1,
            around: Day.today(),
            focus: 0.49999
        }, null);


        // dispatch('ads/fetch', null, ROOT_DISPATCH)
    },

    setCalendar({ state, commit, dispatch }: any, payload: any) {

    },

    // forType: ({ state, commit, dispatch }: any, payload: { type: Units, size: 1, around: Day, focus: number, input?: any; }): CalendarDay[] => {
    //     // console.log({ payload });

    //     const meta: any = Meta[payload.type];

    //     // console.log(payload.around, payload.size, payload.focus);


    //     const start: Day = meta.getStart(payload.around, payload.size, payload.focus);
    //     // console.log({ start });

    //     const end: Day = meta.getEnd(start, payload.size, payload.focus);
    //     // console.log({ end });



    //     // return {start, end, type, size, meta.moveStart, meta.moveEnd, input || meta.defaultInput};
    //     // const minimumSize = fn.coalesce(payload.input.minimumSize, 0);
    //     const minimumSize = 0;
    //     // console.log(fn.coalesce(input.minimumSize, 0));

    //     const span = new DaySpan(start, end);
    //     // console.log(start, end);

    //     const filled = new DaySpan(start, end);
    //     const days: CalendarDay[] = [];
    //     const daysBetween: number = filled.days(Op.UP);
    //     const total: number = Math.max(minimumSize, daysBetween);
    //     let current: Day = filled.start;
    //     // console.log(start, end, span, days);


    //     for (let i = 0; i < total; i++) {
    //         let day: CalendarDay = days[i];

    //         if (!day || !day.sameDay(current)) {
    //             day = new CalendarDay(current.date);

    //             if (i < days.length) {
    //                 days.splice(i, 1, day);
    //             }
    //             else {
    //                 days.push(day);
    //             }
    //         }

    //         day.inCalendar = span.contains(day);

    //         current = current.next();
    //     }
    //     if (days.length > total) {
    //         days.splice(total, days.length - total);
    //     }
    //     /**
    //     * Updates the days with the current day via [[CalendarDay.updateCurrent]].
    //     *
    //     * @param Day.today() The new current day.
    //     */
    //     iterateDays(days).each(d => d.updateCurrent(Day.today()));


    //     commit('SET_DAYS', days);

    //     // console.log('SET_DAYS', { days });
    //     // console.log('DAYS_LENGTH', days.length);


    //     return days;
    // },

    getWeekViewDays: ({ state, dispatch }: any, payload?: { around: Day; }): any[] => {
        return dispatch('forType', ({ type: Units.WEEK, size: 1, around: payload?.around ? payload.around : Day.today(), focus: 0.4999, input: undefined }), null);
    },

};

const getters = {
    getDayViewDays: ({ state, dispatch }: any) => (days: number = 1, around: Day = Day.today(), focus: number = 0.4999, input?: any): any[] => {
        return dispatch('forType', ({ type: Units.DAY, size: days, around: around, focus, input }), null);
    },
    getMonthViewDays: ({ state, dispatch }: any) => (months: number = 1, around: Day = Day.today(), focus: number = 0.4999, input?: any): any[] => {
        return dispatch('forType', ({ type: Units.MONTH, size: months, around: around, focus, input }), null);
    },

    getYearViewDays: ({ state, dispatch }: any) => (years: number = 1, around: Day = Day.today(), focus: number = 0.4999, input?: any): any[] => {
        return dispatch('forType', ({ type: Units.MONTH, size: years, around: around, focus, input }), null);
    },

    getScheduleViewDays: ({ state, dispatch }: any) => (scheduleDays: number = 1, around: Day = Day.today(), focus: number = 0.0000, input?: any): any[] => {
        return dispatch('forType', ({ type: Units.DAY, size: scheduleDays, around: around, focus, input }), null);
    },

    ...make.getters(state)

};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};
