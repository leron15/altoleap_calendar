// Utilities
// import { differenceInDays } from 'date-fns'
import { make } from 'vuex-pathify';

// Globals
import { IN_BROWSER } from '@/util/globals';

const state = {
  dark: true,
  drawer: {
    advanced: false,
    mini: false,
  },
  notifications: [],
  rtl: false,
  snackbar: Date.now(),
};

const mutations = make.mutations(state);

const actions = {
  fetch: ({ commit }: any) => {
    const local = localStorage.getItem('vuetify@user') || '{}';
    const user = JSON.parse(local);

    for (const key in user) {
      commit(key, user[key]);
    }
  },
  update: ({ state }: any) => {
    if (!IN_BROWSER) return;

    localStorage.setItem('vuetify@user', JSON.stringify(state));
  },
};

const getters = {
  // hasRecentlyViewed: (_: any, __: any, rootState: any) => {
  //   const last = rootState.user.snackbar

  //   if (!last) return false

  //   return differenceInDays(Date.now(), Number(last)) < 2
  // },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
