// Utilities
import { make } from "vuex-pathify";
import { ROOT_DISPATCH } from '@/store';


const state = {
  currentVersion: process.env.VUE_APP_VERSION,
  drawer: true,
  rightDrawer: false,
  isLoading: false,
  releases: [],
  companyName: 'Flick',
  links: require('@/data/drawerItems.json'),

};

const mutations = make.mutations(state);

const actions = {
  init: async ({ dispatch }: any) => {
    // dispatch('messages/fetch', null, ROOT_DISPATCH)
    dispatch('user/fetch', null, ROOT_DISPATCH);
    // dispatch('ads/fetch', null, ROOT_DISPATCH)
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
