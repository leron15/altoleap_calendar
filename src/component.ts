import { Day, Constants } from 'altoleap-calendar-lib/src';

export default {

  data:
  {
    version: '0.3.0',

    readOnly: false,

    title: 'Altoleap Calendar',

    today: Day.today(),
    tomorrow: Day.tomorrow(),
    now: Day.now(),
    timeout: null as any,
    refreshInterval: Constants.MILLIS_IN_MINUTE,


    // interval Values
    intervalHeight: 48,
    intervalMinutes: 60,
    intervalWidth: 60,
    intervalCount: 24,

    // prompt
    prompt: {
      actionRemove: true,
      actionExclude: true,
      actionCancel: true,
      actionUncancel: true,
      actionMove: true,
      actionInclude: true,
      actionSetStart: true,
      actionSetEnd: true,
      move: true,
      toggleAllDay: true,
      removeExistingTime: true
    },

    promptOpen: null,

    promptLabels: {
      actionRemove: 'Are you sure you want to remove this event?',
      actionExclude: 'Are you sure you want to remove this event occurrence?',
      actionCancel: 'Are you sure you want to cancel this event?',
      actionUncancel: 'Are you sure you want to uncancel this event?',
      actionSetStart: 'Are you sure you want to set this occurrence as the first?',
      actionSetEnd: 'Are you sure you want to set this occurrence as the last?',
      actionMove: 'Are you sure you want to move this event?',
      actionInclude: 'Are you sure you want to add an event occurrence?',
      move: 'Are you sure you want to move this event?',
      toggleAllDay: 'Are you sure you want to change whether this event occurs all day?',
      removeExistingTime: 'Are you sure you want to remove all event occurrences at this time?'
    },

    placeholder: {
      noTitle: '(no title)'
    },
  },
  computed: {
    bodyHeight(): number {
      return (this as any).intervalCount * (this as any).intervalHeight;
    },
    nowLineStyleBorder(): string | object {
      const now = (this as any).now.asTime().toMilliseconds();
      const delta = now / Constants.MILLIS_IN_DAY;
      const top = delta * (this as any).bodyHeight;

      return {
        position: "absolute",
        left: "0px",
        right: "-1px",
        top: top - 1 + "px",
        // borderTop: this.getStyleNowBorder()
      };
    }
  },
  methods:
  {
    getDefaultEventDetails() {
      return {
        title: '',
        description: '',
        location: '',
        // color: this.getDefaultEventColor(),
        forecolor: '#ffffff',
        calendar: '',
        busy: true,
        icon: 'mdi-calendar'
      };
    },
    init() {
      console.log('Initialized Altoleap Calendar', this.bodyHeight);

      (this as any).startRefreshTimes();
    },

    // times
    startRefreshTimes() {
      const $altoleapCalendar = this;

      (this as any).timeout = setTimeout(
        function () {
          $altoleapCalendar.refreshTimes();
          $altoleapCalendar.startRefreshTimes();
        },
        (this as any).refreshInterval
      );
    },
    refreshTimes(force = false) {
      const today = Day.today();

      if (!today.sameDay((this as any).today) || force) {
        (this as any).today = today;
        (this as any).tomorrow = Day.tomorrow();
      }

      (this as any).now = Day.now();
    },

  }
};