// Mixins
import CalendarBase from './base';
import mixins from '@/util/mixins';
import Times from './times';
import Localable from './localable';
import Mouse from './mouse';

// Util
import props from '@/util/calendar/props';
import {
    parseTime,
    copyTimestamp,
    updateMinutes,
    // createDayList,
    // createIntervalList,
    createNativeLocaleFormatter,
    VTime,
    MINUTES_IN_DAY
} from '@/util/calendar/timestamp';

import {
    getWeekdaySkips,
    getTimestampIdentifier,
    createIntervalList,
    createDayList,
} from '@/util/newCalendar';
import {
    Day, DaySpan,
    Functions as fn,
    Units,
    CalendarDay,
    Calendar
} from 'altoleap-calendar-lib/src';

import { CalendarTimestamp, CalendarFormatter, CalendarDayBodySlotScope } from 'vuetify/types';
// import { isString } from 'lodash';
import { call, get } from 'vuex-pathify';



export default mixins(
    Localable,
    Mouse,
    Times,
    // CalendarBase
    // Colorable
    /* @vue/component */
).extend({
    name: 'calendar-with-intervals',

    props: {
        calendar: {
            required: true,
            type: Calendar
        },
        placeholder: {
            type: [Object]
        },

        placeholderForCreate: {
            type: Boolean,
            default: false
        },
        ...props.intervals,
    },

    computed: {

        // days: get<CalendarDay[]>('calendar/days'),

        parsedFirstInterval(): number {
            return fn.isString(this.firstInterval) ? parseInt(this.firstInterval) : this.firstInterval;
        },
        parsedIntervalMinutes(): number {
            return fn.isString(this.$altoleapCalendar.intervalMinutes) ? parseInt(this.$altoleapCalendar.intervalMinutes) : this.$altoleapCalendar.intervalMinutes;
        },
        parsedIntervalCount(): number {
            return fn.isString(this.$altoleapCalendar.intervalCount) ? parseInt(this.$altoleapCalendar.intervalCount) : this.$altoleapCalendar.intervalCount;

        },
        parsedIntervalHeight(): number {
            return fn.isString(this.$altoleapCalendar.intervalHeight) ? parseInt(this.$altoleapCalendar.intervalHeight) : this.$altoleapCalendar.intervalHeight;

        },
        parsedFirstTime(): number | false {
            return parseTime(this.firstTime);
        },
        firstMinute(): number {
            const time = this.parsedFirstTime;

            return time !== false && time >= 0 && time <= MINUTES_IN_DAY
                ? time
                : this.parsedFirstInterval * this.parsedIntervalMinutes;
        },
        bodyHeight(): number {
            return this.$altoleapCalendar ? this.$altoleapCalendar.bodyHeight : this.parsedIntervalCount * this.parsedIntervalHeight;
        },
        intervals(): Day[][] {
            const days: Day[] = this.calendar.days;
            const first: number = this.firstMinute;
            const minutes: number = this.parsedIntervalMinutes;
            const count: number = this.parsedIntervalCount;
            const now: Day = this.times.now;


            return days.map(d => createIntervalList(d, first, minutes, count, now));
        },
        intervalFormatter(): CalendarFormatter {
            if (this.intervalFormat) {
                return this.intervalFormat as CalendarFormatter;
            }

            const longOptions = { timeZone: 'UTC', hour: '2-digit', minute: '2-digit' };
            const shortOptions = { timeZone: 'UTC', hour: 'numeric', minute: '2-digit' };
            const shortHourOptions = { timeZone: 'UTC', hour: 'numeric' };

            return createNativeLocaleFormatter(
                this.currentLocale,
                (tms, short) => short ? (tms.minute === 0 ? shortHourOptions : shortOptions) : longOptions
            );
        },
    },
    methods: {

        // forType: call('calendar/forType'),

        showIntervalLabelDefault(interval: CalendarTimestamp): boolean {
            const first: CalendarTimestamp = this.intervals[0][0];
            const isFirst: boolean = first.hour === interval.hour && first.minute === interval.minute;
            return !isFirst;
        },
        intervalStyleDefault(_interval: CalendarTimestamp): object | undefined {
            return undefined;
        },

        scrollToTime(time: Day): boolean {
            const y = this.timeToY(time);
            const pane = this.$refs.scrollArea as HTMLElement;

            if (y === false || !pane) {
                return false;
            }

            pane.scrollTop = y;

            return true;
        },
        minutesToPixels(minutes: number): number {
            return minutes / this.parsedIntervalMinutes * this.parsedIntervalHeight;
        },
        timeToY(time: Day, clamp = true): number | false {
            let y = this.timeDelta(time);

            if (y !== false) {
                y *= this.bodyHeight;

                if (clamp) {
                    if (y < 0) {
                        y = 0;
                    }
                    if (y > this.bodyHeight) {
                        y = this.bodyHeight;
                    }
                }
            }

            return y;
        },
        timeDelta(time: Day): number | false {
            const minutes = parseTime(time.format('hh:mm:ss'));

            if (minutes === false) {
                return false;
            }

            const min: number = this.firstMinute;
            const gap: number = this.parsedIntervalCount * this.parsedIntervalMinutes;

            return (minutes - min) / gap;
        },
    },

    mounted() {
        // this.forType({ type: Units.DAY, size: 1, around: this.times.today, focus: 0.4999 });
    }
});