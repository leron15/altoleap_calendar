import Vue from 'vue';

import {
    validateTimestamp,
    parseTimestamp,
    parseDate,
} from '@/util/calendar/timestamp';
import { CalendarTimestamp } from 'vuetify/types';

import {
    Day, DaySpan,
    getWeekdaySkips,
    getTimestampIdentifier,
    createDayList,
    Functions as fn,
    parseDay
} from 'altoleap-calendar-lib/src';


export default Vue.extend({
    name: 'times',

    props: {
        now: {
            type: String,
            validator: validateTimestamp,
        },
    },

    data: () => ({
        times: {
            now: Day.now(),
            today: Day.today(),
        },
        interval: null as any
    }),

    computed: {
        parsedNow(): Day | null {
            return this.now ? parseDay(this.now, true) : null;
        },
    },

    watch: {
        parsedNow: 'updateTimes',
    },

    created() {
        this.updateTimes();
    },
    mounted() {
        this.interval = setInterval(() => this.updateTimes(), 60 * 1000);
    },

    beforeDestroy() {
        this.stop();
    },

    methods: {
        stop() {
            clearInterval(this.interval);
            this.interval = null;
        },
        updateTimes(): void {
            const now: Day = this.parsedNow || this.getNow();
            this.updateDay(now, this.times.now);
            this.updateTime(now, this.times.now);
            // this.updateDay(now, this.times.today);
        },
        getNow(): Day {
            return Day.now();
        },
        updateDay(now: Day, target: Day): void {
            if (now.date !== target.date) {
                target.year = now.year;
                target.month = now.month;
                target.day = now.day;
                target.weekday = now.weekday;
                target.date = now.date;
            }
        },
        updateTime(now: Day, target: Day): void {
            if (now.sameHour(target) && !now.sameMinute(target)) {
                this.times.now = now;
            }
        },
    },
});
