
import Mouse from './mouse';
import Times from './times';
import Localable from './localable';
import Colorable from './colorable';
import mixins from '@/util/mixins';
import props from '@/util/calendar/props';

import {
  parseTimestamp,
  // getWeekdaySkips,
  // createDayList,
  createNativeLocaleFormatter,
  // getStartOfWeek,
  // getEndOfWeek,
  // getTimestampIdentifier,
} from '@/util/calendar/timestamp';
import {
  Calendar,
  Day, DaySpan,
  getWeekdaySkips,
  getTimestampIdentifier,
  createDayList,
  Unit,
  Units,
  CalendarInput,
  CalendarDay
} from 'altoleap-calendar-lib/src';

import { CalendarTimestamp, CalendarFormatter } from 'vuetify/types';



import { sync, get } from 'vuex-pathify';
export default mixins(
  Localable,
  Mouse,
  Times,
  Colorable
  /* @vue/component */
).extend({
  name: 'calendar-base',

  props: {
    calendar: {
      type: Calendar,
      default() {
        return Calendar.months();
      },
    },
    ...props.base,
  },

  data: () => ({


    types: [
      {
        id: "D",
        label: "Day",
        shortcut: "D",
        type: Units.DAY,
        size: 1,
        focus: 0.4999,
        repeat: true,
        listTimes: true,
        updateRows: true,
        schedule: false,
      },
      {
        id: "W",
        label: "Week",
        shortcut: "W",
        type: Units.WEEK,
        size: 1,
        focus: 0.4999,
        repeat: true,
        listTimes: true,
        updateRows: true,
        schedule: false,
      },
      {
        id: "M",
        label: "Month",
        shortcut: "M",
        type: Units.MONTH,
        size: 1,
        focus: 0.4999,
        repeat: true,
        listTimes: false,
        updateRows: true,
        schedule: false,
      },
      {
        id: "Y",
        label: "Year",
        shortcut: "Y",
        type: Units.YEAR,
        size: 1,
        focus: 0.4999,
        repeat: true,
        listTimes: false,
        updateRows: true,
        schedule: false,
      },
      {
        id: "S",
        label: "Schedule",
        shortcut: "S",
        type: Units.DAY,
        size: 92,
        focus: 0.0,
        repeat: false,
        listTimes: false,
        updateRows: false,
        schedule: true,
      },
      {
        id: "X",
        label: "Custom",
        shortcut: "X",
        type: Units.DAY,
        size: 4,
        focus: 0.4999,
        repeat: true,
        listTimes: true,
        updateRows: true,
        schedule: false,
      },
    ],


  }),

  computed: {
    currentType: {
      get() {
        return (
          this.types.find(
            (type: any) =>
              type.type === this.calendar.type &&
              type.size === this.calendar.size
          ) || this.types[0]
        );
      },
      set(type: any) {
        this.rebuild(undefined, true, type);
      },
    } as any,

    // summary() {
    //   const small = this.$vuetify.breakpoint.xs;

    //   if (small) {
    //     return this.calendar.start.format(this.formats.xs);
    //   }

    //   const large = this.$vuetify.breakpoint.mdAndUp;

    //   return this.calendar.summary(false, !large, false, !large);
    // },


    // calendar: sync<Calendar>('calendar/calendar'),
    today: get<Day>('calendar/today'),
    // type: sync<Units>('calendar/type'),
    // type: sync<Units>('calendar/type'),
    // start: sync<Day>('calendar/start'),


    days(): CalendarDay[] {
      return this.calendar ? this.calendar.days : [];
    },

    type(): Units | null {
      return this.calendar ? this.calendar.type : null;
    },

    size(): number | null {
      return this.calendar ? this.calendar.size : null;
    },

    isDay(): boolean {
      return this.type === Units.DAY;
    },

    isWeek(): boolean {
      return this.type === Units.WEEK;
    },

    isMonth(): boolean {
      return this.type === Units.MONTH;
    },

    isYear(): boolean {
      return this.type === Units.YEAR;
    },





    parsedWeekdays(): number[] {
      return Array.isArray(this.weekdays)
        ? this.weekdays
        : (this.weekdays || '').split(',').map(x => parseInt(x, 10));
    },
    weekdaySkips(): number[] {
      return getWeekdaySkips(this.parsedWeekdays);
    },
    // weekdaySkipsReverse(): number[] {
    // 	const reversed = this.weekdaySkips.slice();
    // 	reversed.reverse();
    // 	return reversed;
    // },
    // parsedStart(): Day {
    // 	return this.start;
    // },
    // parsedEnd(): Day {
    // 	const start = this.start;
    // 	const end: Day = this.end ? this.end || start : start;

    // 	return getTimestampIdentifier(end) < getTimestampIdentifier(start) ? start : end;
    // },
    // days(): Day[] {
    // 	return createDayList(
    // 		this.parsedStart,
    // 		this.parsedEnd,
    // 		Day.today(),
    // 		this.weekdaySkips
    // 	);
    // },
    // dayFormatter(): CalendarFormatter {
    // 	if (this.dayFormat) {
    // 		return this.dayFormat as CalendarFormatter;
    // 	}

    // 	const options = { timeZone: 'UTC', day: 'numeric' };

    // 	return createNativeLocaleFormatter(
    // 		this.currentLocale,
    // 		(_tms, _short) => options
    // 	);
    // },
    // weekdayFormatter(): CalendarFormatter {
    // 	if (this.weekdayFormat) {
    // 		return this.weekdayFormat as CalendarFormatter;
    // 	}

    // 	const longOptions = { timeZone: 'UTC', weekday: 'long' };
    // 	const shortOptions = { timeZone: 'UTC', weekday: 'short' };

    // 	return createNativeLocaleFormatter(
    // 		this.currentLocale,
    // 		(_tms, short) => short ? shortOptions : longOptions
    // 	);
    // },
  },

  methods: {

    // setState(state: CalendarInput<any>) {
    //   // state.eventSorter = state.listTimes
    //   //   ? Sorts.List([Sorts.FullDay, Sorts.Start])
    //   //   : Sorts.Start;


    //   this.calendar.set(state);

    //   // this.triggerChange();
    // },

    //   applyEvents() {
    // 	if (this.events) {
    // 	  this.calendar.removeEvents();
    // 	  this.calendar.addEvents(this.events);
    // 	}
    //   },

    // isType(type: any, aroundDay: CalendarDay): any {
    //   const cal = this.calendar;

    //   return (
    //     cal.type === type.type &&
    //     cal.size === type.size &&
    //     (!aroundDay || cal.span.matchesDay(aroundDay))
    //   );
    // },

    // rebuild(aroundDay: CalendarDay, force: boolean, forceType: any) {
    //   const type = forceType || this.currentType || this.types[2];

    //   if (this.isType(type, aroundDay) && !force) {
    //     return;
    //   }

    //   const input = {
    //     type: type.type,
    //     size: type.size,
    //     around: aroundDay,
    //     eventsOutside: true,
    //     preferToday: false,
    //     listTimes: type.listTimes,
    //     updateRows: type.updateRows,
    //     updateColumns: type.listTimes,
    //     fill: !type.listTimes,
    //     otherwiseFocus: type.focus,
    //     repeatCovers: type.repeat,
    //   };

    //   this.setState(input);
    //   this.calendar.setEvents(this.defaultEvents, true);
    // },

    refreshCurrent() {
      this.calendar.refreshCurrent(this.today);
    },

    getRelativeClasses(timestamp: CalendarTimestamp, outside = false): object {
      return {
        'v-present': timestamp.present,
        'v-past': timestamp.past,
        'v-future': timestamp.future,
        'v-outside': outside,
      };
    },
    getFormatter(options: object): CalendarFormatter {
      return createNativeLocaleFormatter(
        this.locale,
        (_tms, _short) => options
      );
    },
  },

});