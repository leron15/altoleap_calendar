import Vue from "vue";
import "./registerServiceWorker";


import { createRouter } from "@/router";
import { createStore } from "@/store";


import { createVuetify } from "@/plugins/vuetify";

import { registerPlugins } from '@/plugins';

// Application
import App from './App.vue';


Vue.config.productionTip = false;

registerPlugins(Vue);
export function dsBind(bind: any) {
  return function (data: any, tag: any, value: any, asProp: any, isSync: any): any {
    if (value && value.$scopedSlots) {
      data.scopedSlots = value.$scopedSlots;
      delete value.$scopedSlots;
    }

    return bind.apply(this, arguments);
  };
}

Vue.prototype._b = dsBind(Vue.prototype._b);

const store = createStore();

const vuetify = createVuetify(store);
const router = createRouter(vuetify, store, undefined);

import AltoleapCalendar from './plugin';

Vue.use(AltoleapCalendar, {});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
