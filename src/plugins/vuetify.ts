
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "@mdi/font/css/materialdesignicons.css";
import { VueConstructor } from 'vue/types/umd';

export function createVuetify(store: any) {
  return new Vuetify({
    theme: {
      default: 'light',
      disable: false,
      options: {
        customProperties: true,
        minifyTheme: (css: string) => {
          return process.env.NODE_ENV === "production"
            ? css.replace(/[\r\n|\r|\n]/g, "")
            : css;
        }
      },
      dark: !store.state.user.dark,

      themes: {
        light: {
          primary: '#1976D2',
          secondary: '#424242',
          accent: '#82B1FF',
          error: '#FF5252',
          info: '#2196F3',
          success: '#4CAF50',
          warning: '#FFC107',
        },
        dark: {
          primary: '#1976D2',
          secondary: '#424242',
          accent: '#82B1FF',
          error: '#FF5252',
          info: '#2196F3',
          success: '#4CAF50',
          warning: '#FFC107',
        }
      }
    }
  });
}

export function useVuetify(Vue: VueConstructor) {
  Vue.use(Vuetify);
}
