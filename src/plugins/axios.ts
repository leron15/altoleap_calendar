'use strict';

import Vue from 'vue';
import axios, { AxiosStatic, AxiosResponse } from 'axios';
// Full config:  https://github.com/axios/axios#request-config
const token = `Bearer ${localStorage.getItem('access__token')}`;
axios.defaults.headers.common['Authorization'] = token;

const config = {
    baseURL:
        process.env.NODE_ENV === 'production'
            ? process.env.VUE_APP_API_URL
            : process.env.VUE_APP_DEV_API_URL || '',
    timeout: 10 * 1000, // Timeout
    // withCredentials: true, // Check cross-site Access-Control
    headers: {
        'Content-Type': 'application/json'
    }
};
const _axios = axios.create(config);

Vue.use({
    install() {
        Vue.prototype.$axios = _axios;
    }
});

declare module 'vue/types/vue' {
    interface Vue {
        axios: AxiosStatic;
    }
}

export { _axios as axios, AxiosResponse };
