import { VueConstructor } from 'vue/types/umd';
import { registerComponents } from './app';
// import { registerAxios } from './axios';
import { useVuetify } from './vuetify';
export function registerPlugins(Vue: VueConstructor) {
    registerComponents(Vue);
    // registerAxios(Vue);
    // loadFonts(Vue)
    // useMeta(Vue)
    useVuetify(Vue);

}