module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/typescript/recommended",
    "@vue/prettier",
    "@vue/prettier/@typescript-eslint"
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "@typescript-eslint/no-use-before-define": process.env.NODE_ENV === "production" ? "warn" : "off",
    "@typescript-eslint/no-inferrable-types": process.env.NODE_ENV === "production" ? "warn" : "off",
    '@typescript-eslint/no-empty-function': process.env.NODE_ENV === "production" ? "warn" : "off",
    '@typescript-eslint/no-explicit-any': process.env.NODE_ENV === "production" ? "warn" : "off",
    '@typescript-eslint/no-non-null-assertion': process.env.NODE_ENV === "production" ? "warn" : "off",
    '@typescript-eslint/camelcase': process.env.NODE_ENV === "production" ? "warn" : "off",
    '@typescript-eslint/no-unused-vars':
      process.env.NODE_ENV === 'production' ? 'off' : 'off',
    '@typescript-eslint/no-this-alias': process.env.NODE_ENV === "production" ? "warn" : "off",
    'prettier/prettier': process.env.NODE_ENV === "production" ? "warn" : "off",
    'prefer-rest-params': process.env.NODE_ENV === "production" ? "warn" : "off",
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    }
  ]
};
