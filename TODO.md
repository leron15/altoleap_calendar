<!-- FIX ME -->
LEGEND: {
    - : possible problems
    * : What works
    $ : How to determine the bug and remove recurrences of similar bugs
}

`mouseMove` Event is called whenever the mouse left click is held on an existing event and dragged within the Calendar area slots 

Problem: Events past 12pm look behind by 8 hour on utc time when using `mouseMove` Event dont work as intended:
    - look into Event time?
    - look Calendar day object. Not using UTC time?
    * Events that start and end before 12 PM, dont cause this bug
    * Dragging event past 12 PM,  dont cause this bug 
    $ Am Confused with `UTC` ad `LOCAL` time, Need to look into it
    $ UTC by 8 hours and LOCAL by 4 hours. Confusing?
    $ Use of UTC and Local time not consistent? possibly what is causing the error

